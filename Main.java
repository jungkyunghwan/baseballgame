/*
public class Main {
	public static void main(String[] args) {
		// TODO Auto-generated method stub
	}

	 (non-Java-doc)
	 * @see java.lang.Object#Object()
	 
	public Main() {
		super();
	}

}*/

import java.util.Scanner;
import java.util.Random;


public class Main 
{

	public static void main(String[] args) 
	{
		int input_num1 = 0;//사용자 입력변수1
		int controlnum = 0;//for문 제어 숫자
		int random_num = 0, one = 0, two = 0,  three = 0;//변수선언
		int countnum = 0;
		int one_anw = 0, two_anw = 0, three_anw = 0;
		int stCount = 0, baCount = 0;
		int ifnum = 0;
		int counter = 13;
		Random random = new Random();//랜덤숫자 선언
		Scanner input = new Scanner(System.in);//스캔 선언
		
		System.out.println("야구게임입니다. 0~9까지 숫자가 올수있습니다.");
		System.out.println("3자리 숫자를 입력해주세요");
		
		while(controlnum < 10000)
				{
					random_num = random.nextInt(999);//0~999까지 아무숫자나 나온다.
					one = num1(random_num);
					two = num10(random_num);
					three = num100(random_num);
					
					
					
					
					
					if(one != two && two != three && one !=three && random_num > 100)//반복 탈출문
					{
						System.out.println("one : "+one);
						System.out.println("two : "+two);
						System.out.println("three : "+three);
						controlnum = 20000;
					}
						
					
					
				}
		System.out.println("현재 랜덤숫자는 "+random_num+"입니다.");
		
		controlnum = 0 ;
		
		while(controlnum < 10000)
		{
			stCount = 0;
			baCount = 0;
			ifnum = 0;
			while(ifnum < 1)
			{
				System.out.println("숫자를 입력해주세요 : ");
				input_num1 = input.nextInt();
				
				one_anw = num1(input_num1);
				two_anw = num10(input_num1);
				three_anw = num100(input_num1);
				
				if(input_num1 > 99 && input_num1 < 1000 && one_anw != two_anw &&
						two_anw != three_anw && three_anw != one_anw)
				{
					System.out.println("숫자는 100~999까지 입력가능합니다.");
					ifnum = 10 ;
					break;
				}
				System.out.println("숫자는 100~999까지 가능하고  중복된숫자는 금지됩니다.");
			}
			
			
			
			if(one == one_anw)//스트라이크 체크
			{
				stCount++;
			}
			if(two == two_anw)//스트라이크 체크
			{
				stCount++;
			}
			if(three == three_anw)//스트라이크 체크
			{
				stCount++;
			}
			
			if(one == two_anw)//볼체크
			{
				baCount++;
			}
			if(two == three_anw)//볼체크
			{
				baCount++;
			}
			if(three == one_anw)//볼체크
			{
				baCount++;
			}
			
			if(one == three_anw)//볼체크
			{
				baCount++;
			}
			if(two == one_anw)//볼체크
			{
				baCount++;
			}
			if(three == two_anw)//볼체크
			{
				baCount++;
			}
			
			
			counter--;
			System.out.println("스트라이크 : " + stCount + "   볼 : " + baCount );
			System.out.println(counter + "번 남으셨습니다. ");
			
			
			if(counter == 0)
			{
				System.out.println("정답 : "+random_num+"패배 하였습니다 ㅠㅡㅠ ");
				System.out.println("프로그램이 종료됩니다.");
				break;
				
			}
			if(stCount == 3)
			{
				System.out.println("정답 : "+random_num+"클리어 성공 축하합니다 ");
				System.out.println("프로그램이 종료됩니다.");
				break;
			}
			
			
		}
		
		
	
	}
	
//여기부터는 소스 메소드
	static int num1(int a)//함수에 숫자를 넣으면 일의자리 숫자가 반환된다.
	{
		int b;
		b = a%10;
		return b;
	}
	static int num10(int a)//함수에 숫자를 넣으면 십의자리 숫자가 반환된다.
	{
		int b,c;
		b = a % 10;
		c = a % 100;
		return (c-b)/10;
	}
	static int num100(int a)//함수에 숫자를 넣으면 백의자리 숫자가 반환된다.
	{
		int c,d;
		
		c = a % 100;
		d = a;
		
		return (d-c)/100;
	}
	

}
